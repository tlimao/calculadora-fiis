import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_states.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/buy_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/export_spreadsheet_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/remove_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/sell_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/update_av_price.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/repositories/fii_repository_impl.dart';
import 'package:calculadora_preco_medio/context/fiis/view/fii_info.dart';
import 'package:calculadora_preco_medio/context/fiis/view/fii_operation_edit_widget.dart';
import 'package:calculadora_preco_medio/context/fiis/view/fiis_operation_widget.dart';
import 'package:calculadora_preco_medio/context/fiis/view/fiis_widget.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/b3/application/usecases/calculate_b3_taxes.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/db/app_db.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operations_bloc.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operations_states.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/edit_operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/recover_operations.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/remove_operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/process_operations.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'infra/datasources/fii_dao.dart';

class FiisModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => FiisRepositoryImpl()),
        Bind((i) => AppDataBase()),
        Bind((i) => FiiDao(i())),
        Bind((i) => OperationDao(i())),
        Bind((i) => RecoverOperations(i())),
        Bind((i) => OperationsBloc(EmptyOperationsState(), i())),
        Bind((i) => FiisListBloc(EmptyFiisListState(), i())),
        Bind((i) => CalculateB3Taxes()),
        Bind((i) => BuyFii(i(), i(), i())),
        Bind((i) => SellFii(i(), i(), i())),
        Bind((i) => RemoveFii(i(), i(), i())),
        Bind((i) => ExportSpreadsheetFii(i(), i(), i())),
        Bind((i) => UpdateAvPrice(i(), i())),
        Bind((i) => EditOperation(i())),
        Bind((i) => ProcessOperations()),
        Bind((i) => RemoveOperation(i()))
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (_, ___) => FiisWidget(),
        ),
        ModularRouter(
          '/operation',
          child: (_, args) => FiisOperationWidget(operationType: args.data),
        ),
        ModularRouter(
          '/info',
          child: (_, args) => FiiInfo(fii: args.data),
        ),
        ModularRouter(
          '/operation/edit',
          child: (_, args) => FiisOperationEditWidget(operation: args.data),
        ),
      ];

  static Inject get to => Inject<FiisModule>.of();
}
