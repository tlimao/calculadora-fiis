import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_events.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/application/usecases/sell_asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/errors/asset_errors.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/errors/operation_errors.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class SellFii extends SellAsset {
  final FiiDao _fiiDao;
  final OperationDao _operationDao;
  final FiisListBloc _bloc;

  SellFii(this._fiiDao, this._operationDao, this._bloc);

  Future<Map<String, double>> call(Operation operation) async {
    // Tenho o ativo?
    final result = await _fiiDao.select(operation.ticker);

    if (result.isNotEmpty) {
      // Se sim:
      final oldFiiPosition = result[0];
      // Tenho a quantidade operada em carteira?
      if (oldFiiPosition.qnt >= operation.qnt) {
        // Se sim, operar:
        // 1) Atualizar ativo na carteira, não altera preço médio
        final newQnt = oldFiiPosition.qnt - operation.qnt;
        final newPosition = Fii.fromData(
          ticker: oldFiiPosition.ticker,
          qnt: newQnt,
          price: oldFiiPosition.price,
        );
        await _fiiDao.update(newPosition);
        // 2) Calcular resultado da operação
        final profitLoss = roundDouble(
            (operation.price - oldFiiPosition.price) * operation.qnt, 2);
        // 3) Imposto da operação
        final tax = calculateTaxes(profitLoss);
        // 4) Salvar operação
        await _operationDao.insert(operation);
        // 5) Atualizar dados da lista de Fiis
        _bloc.add(LoadFiisListEvent());
        return {
          "result": profitLoss,
          "tax": tax,
        };
      } else {
        throw OperationNotPerformed(
            msg: "Você está tentando vender uma posição maior do que possui!");
      }
    } else {
      throw AssetNotFound(msg: "O Fii não consta na sua carteira!");
    }
  }

  @override
  double calculateTaxes(double result) {
    return (result > 0) ? roundDouble(0.2 * result, 2) : 0;
  }
}
