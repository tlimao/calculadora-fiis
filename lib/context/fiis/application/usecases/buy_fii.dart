import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_events.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/application/usecases/buy_asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class BuyFii extends BuyAsset {
  final FiiDao _fiiDao;
  final OperationDao _operationDao;
  final FiisListBloc _bloc;

  BuyFii(this._bloc, this._fiiDao, this._operationDao);

  call(Operation operation) async {
    // Selecionar o Fii no Banco
    final localAsset = await _fiiDao.select(operation.ticker);

    // Tenho o Fii
    if (localAsset != null && localAsset.length > 0) {
      // Calcular nova posição e preço médio
      final newAvPriceAnd = this.calculateAvPrice(localAsset[0], operation);
      // Atualizar o Fii no Bloc
      Fii fii = Fii.fromData(
        ticker: operation.ticker,
        qnt: newAvPriceAnd['qnt'],
        price: newAvPriceAnd['price'],
      );
      await _fiiDao.update(fii);
    } else {
      // Não tenho o Fii
      Fii fii = Fii.fromOperation(operation);
      await _fiiDao.insert(fii);
    }
    // Salvar Operação
    await _operationDao.insert(operation);
    // Atualizar o UI
    _bloc.add(LoadFiisListEvent());
  }
}
