import "dart:io";

import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/b3/application/usecases/calculate_b3_taxes.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class ExportSpreadsheetFii {
  final FiiDao _fiiDao;
  final OperationDao _operationDao;
  final CalculateB3Taxes _taxes;

  ExportSpreadsheetFii(this._operationDao, this._taxes, this._fiiDao);

  Future<File> call(String ticker) async {
    Directory appDocDirectory = await getTemporaryDirectory();

    final datetime = DateFormat('dd-MM-yyyy mm:hh').format(DateTime.now());

    File spreadsheet = File(appDocDirectory.path + '/$ticker $datetime.csv');

    final operations = await _operationDao.select(ticker);

    final result = await _fiiDao.select(ticker);
    final fii = result[0];

    String header =
        "Ativo;$ticker; Posicao; ${fii.qnt.round()}; PM; ${fii.price};\n\n";

    String lines = "";

    operations.forEach((operation) {
      final verboseOperationType =
          (operation.type == OperationType.buy) ? "Compra" : "Venda";
      lines += "${DateFormat('dd-MM-yyyy').format(operation.date)};" +
          "$verboseOperationType;" +
          "${operation.ticker};" +
          "${operation.qnt.round()};" +
          "${operation.price};" +
          "${roundDouble(operation.price * operation.qnt, 2)};" +
          "${_taxes.fromOperation(operation)['b3_fee']};\n";
    });

    return await spreadsheet.writeAsString(header + lines);
  }
}
