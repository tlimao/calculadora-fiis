import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class UpdateAvPrice {
  final OperationDao _operationDao;
  final FiiDao _fiiDao;

  UpdateAvPrice(this._operationDao, this._fiiDao);

  Future<Fii> call(Fii fii) async {
    var operations = await _operationDao.select("*");
    // Ordenar por data as operações
    operations.sort((o1, o2) => o1.date.compareTo(o2.date));
    // Iniciar variáveis
    double pm = 0;
    double position = 0;
    // Iterar sobre as operações
    operations.forEach(
      (operation) {
        if (operation.type == OperationType.buy) {
          pm = (pm * position + operation.price * operation.qnt) /
              (operation.qnt + position);
          position += operation.qnt;
        }
      },
    );

    Fii newFii = Fii.fromData(
      ticker: fii.ticker,
      qnt: position,
      price: roundDouble(pm, 2),
    );
    await _fiiDao.update(newFii);

    return newFii;
  }
}
