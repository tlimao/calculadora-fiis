import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_events.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class RemoveFii {
  final FiiDao _fiiDao;
  final OperationDao _operationDao;
  final FiisListBloc _bloc;

  RemoveFii(this._fiiDao, this._operationDao, this._bloc);

  call(String ticker) async {
    final result = await _fiiDao.select(ticker);
    // Remover o Fii
    _fiiDao.delete(result[0]);
    // Remover todas as operações do fii
    _operationDao.deleteAssetOperations(ticker);
    // Atualizar a lista de fiis
    _bloc.add(LoadFiisListEvent());
  }
}
