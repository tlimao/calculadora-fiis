import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';

class FiisListState {
  final List<Fii> fiisList;

  FiisListState(this.fiisList);
}

class EmptyFiisListState extends FiisListState {
  EmptyFiisListState() : super([]);
}

class NewFiisListState extends FiisListState {
  NewFiisListState(List<Fii> fiisList) : super(fiisList);
}
