import 'package:bloc/bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_events.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_states.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';

class FiisListBloc extends Bloc<FiisListEvent, FiisListState> {
  final FiiDao _localRepository;

  FiisListBloc(FiisListState initialState, this._localRepository)
      : super(initialState);

  @override
  Stream<FiisListState> mapEventToState(FiisListEvent event) async* {
    if (event is AddFiiListEvent) {
      List<Fii> newFiisList = List<Fii>();
      newFiisList.addAll(state.fiisList);
      newFiisList.add(event.fii);
      yield FiisListState(newFiisList);
    }

    if (event is LoadFiisListEvent) {
      final newFiisList = await _localRepository.select("*");
      yield FiisListState(newFiisList);
    }
  }
}
