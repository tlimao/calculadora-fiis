import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';

abstract class FiisListEvent {
  final Fii fii;
  final OperationType operation;

  FiisListEvent({this.fii, this.operation});
}

class AddFiiListEvent extends FiisListEvent {
  AddFiiListEvent({Fii fii, OperationType operation})
      : super(fii: fii, operation: operation);
}

class LoadFiisListEvent extends FiisListEvent {}
