import 'dart:convert';

import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/repository/fii_repository.dart';
import 'package:flutter/services.dart' show rootBundle;

class FiisRepositoryImpl extends FiisRepository {
  List<Fii> fiisList;

  FiisRepositoryImpl() {
    _load();
  }

  @override
  Future<List<Fii>> get(String keyword) {
    // TODO: implement get
    throw UnimplementedError();
  }

  String getName(String ticker) {
    try {
      return fiisList.firstWhere((element) {
        return (element.ticker == ticker) ? true : false;
      }).name;
    } catch (e) {
      return "Não listado";
    }
    ;
  }

  Future _load() async {
    try {
      fiisList = List<Fii>();
      String jsonString = await rootBundle.loadString('assets/raw/fiis.json');
      Map parsedJson = json.decode(jsonString);
      var categoryJson = parsedJson['fiis'] as List;
      for (int i = 0; i < categoryJson.length; i++) {
        fiisList.add(Fii.fromMap(categoryJson[i]));
      }
    } catch (e) {
      print(e);
    }
  }
}
