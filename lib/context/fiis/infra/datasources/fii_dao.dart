import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/db/app_db.dart';

const String FII_TABLE = "fiiasset";
const String FII_TABLE_SQL = "CREATE TABLE IF NOT EXISTS $FII_TABLE( " +
    "ticker TEXT PRIMARY KEY," +
    "qnt REAL, " +
    "price REAL)";

extension ParseToMap on Fii {
  Map<String, dynamic> toMapDao() {
    return {
      "ticker": this.ticker,
      "qnt": this.qnt,
      "price": this.price,
    };
  }
}

class FiiDao {
  final AppDataBase _dataBase;

  FiiDao(this._dataBase) {
    _dataBase.createTable(FII_TABLE_SQL);
  }

  insert(Fii fii) async {
    final db = await _dataBase.get();

    db.insert(
      FII_TABLE,
      fii.toMapDao(),
    );
  }

  delete(Fii fii) async {
    final db = await _dataBase.get();
    db.delete(
      FII_TABLE,
      where: 'ticker = ?',
      whereArgs: [fii.ticker],
    );
  }

  Future<List<Fii>> select(String keyword) async {
    final db = await _dataBase.get();

    List<Map<String, dynamic>> result;

    if (keyword == "*") {
      result = await db.query(FII_TABLE);
    } else {
      result = await db.query(
        FII_TABLE,
        where: 'ticker = ?',
        whereArgs: [keyword],
      );
    }

    List<Fii> fiis = List<Fii>();

    result.forEach((element) {
      fiis.add(Fii.fromData(
          ticker: element['ticker'],
          qnt: element['qnt'],
          price: element['price'],
          name: "Nome"));
    });

    return fiis;
  }

  update(Fii fii) async {
    final db = await _dataBase.get();
    db.update(
      FII_TABLE,
      {
        'qnt': fii.qnt,
        'price': fii.price,
      },
      where: 'ticker = ?',
      whereArgs: [fii.ticker],
    );
  }
}
