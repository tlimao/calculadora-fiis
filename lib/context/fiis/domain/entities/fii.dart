import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:intl/intl.dart';

enum FiiType { FoF, TIJOLO, HIBRIDO, DESENVOLVIMENTO, DEFAULT }

class Fii extends Asset {
  final FiiType fiiType;
  final DateTime earningsDate;

  Fii({
    this.fiiType,
    this.earningsDate,
    String ticker,
    String name,
    double price,
    double qnt,
    String url,
  }) : super(
          type: AssetType.Fii,
          ticker: ticker,
          name: name,
          price: price,
          qnt: qnt,
          url: url,
        );

  Map<String, dynamic> toMap() {
    Map<String, dynamic> fiiMap = super.toMap();

    fiiMap.addAll({
      'fiiType': EnumToString.convertToString(fiiType),
      'earningsDate': DateFormat('yyyy-MM-dd hh:mm:ss').format(earningsDate),
    });

    return fiiMap;
  }

  factory Fii.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Fii(
      fiiType: (map["fii_type"] != null)
          ? EnumToString.fromString(
              FiiType.values,
              map['fii_type'],
            )
          : FiiType.DEFAULT,
      earningsDate: map['earningsDate'],
      ticker: map['ticker'],
      name: map['name'],
      price: map['price'],
      qnt: map['qnt'],
      url: map['url'],
    );
  }

  factory Fii.fromData({
    String ticker,
    double price,
    double qnt,
    String name,
  }) {
    return Fii(
      fiiType: FiiType.DEFAULT,
      earningsDate: DateTime.parse("2000-01-01 23:59:00"),
      ticker: ticker,
      name: name,
      price: price,
      qnt: qnt,
      url: "",
    );
  }

  factory Fii.fromOperation(Operation operation) {
    return Fii(
      fiiType: FiiType.DEFAULT,
      earningsDate: operation.date,
      ticker: operation.ticker,
      name: "",
      price: operation.price,
      qnt: operation.qnt,
      url: "",
    );
  }
}
