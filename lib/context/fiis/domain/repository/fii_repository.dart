import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/theme/app_theme.dart';

abstract class FiisRepository {
  Future<List<Fii>> get(String keyword);
}
