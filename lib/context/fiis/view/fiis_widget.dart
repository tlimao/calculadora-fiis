import 'package:calculadora_preco_medio/context/fiis/view/fiis_list_widget.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class FiisWidget extends StatefulWidget {
  @override
  _FiisWidgetState createState() => _FiisWidgetState();
}

class _FiisWidgetState extends State<FiisWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.apartment_rounded),
        title: Text("Fiis"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: FiisListWidget(),
            )
          ],
        ),
      ),
      floatingActionButton: SpeedDial(
        // both default to 16
        marginRight: 18,
        marginBottom: 20,
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22.0),
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
        visible: true,
        // If true user is forced to close dial manually
        // by tapping main button and overlay is not rendered.
        closeManually: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        tooltip: 'Speed Dial',
        heroTag: 'fiis-actions',
        backgroundColor: Colors.amberAccent[400],
        foregroundColor: Colors.blueGrey,
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
              child: Icon(Icons.arrow_forward),
              backgroundColor: Colors.lightGreenAccent[400],
              label: 'Cadastrar Compra',
              labelStyle: TextStyle(fontSize: 18.0, color: Colors.black),
              onTap: () => _cadastrarCompra()),
          SpeedDialChild(
              child: Icon(Icons.arrow_back),
              backgroundColor: Colors.redAccent[400],
              label: 'Cadastrar Venda',
              labelStyle: TextStyle(fontSize: 18.0, color: Colors.black),
              onTap: () => _cadastrarVenda()),
        ],
      ),
    );
  }

  _cadastrarCompra() {
    Modular.navigator.pushNamed(
      "/operation",
      arguments: OperationType.buy,
    );
  }

  _cadastrarVenda() {
    Modular.navigator.pushNamed(
      "/operation",
      arguments: OperationType.sell,
    );
  }
}
