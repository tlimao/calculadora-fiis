import 'package:calculadora_preco_medio/context/fiis/application/usecases/update_av_price.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operation_events.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operations_bloc.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operations_states.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/remove_operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/process_operations.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/ui/charts/profit_loss_chart.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/asset/view/asset_card_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

class FiiInfo extends StatefulWidget {
  final Fii fii;

  const FiiInfo({this.fii}) : super();
  @override
  _FiiInfoState createState() => _FiiInfoState();
}

class _FiiInfoState extends State<FiiInfo> {
  final _blocOperations = Modular.get<OperationsBloc>();
  final _processOperationsUsecase = Modular.get<ProcessOperations>();
  final _removeOperationUsecase = Modular.get<RemoveOperation>();
  final _updateAvPriceUsecase = Modular.get<UpdateAvPrice>();
  Fii fii;

  @override
  void initState() {
    fii = widget.fii;
    _blocOperations.add(LoadOperationsEvent(widget.fii.ticker));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${fii.ticker} - Informações"),
      ),
      body: StreamBuilder(
        initialData: EmptyOperationsState(),
        stream: _blocOperations,
        builder: (context, snapshot) {
          if (snapshot == null || snapshot.data == null) {
            return CircularProgressIndicator();
          } else {
            final operations = (snapshot.data as OperationsState).operations;
            return SingleChildScrollView(
              child: Column(
                children: [
                  AssetCardInfo(fii),
                  Container(
                    width: double.maxFinite,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.amber,
                    ),
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "Lucro/Prejuízo Ano",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  _buildProfitLossChart(operations),
                  Container(
                    width: double.maxFinite,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.amber,
                    ),
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "Histórico de Operações",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 400,
                    child: ListView.builder(
                      itemCount: operations.length,
                      itemBuilder: (context, index) {
                        final operation = operations[index];
                        return Card(
                          color: Colors.white54,
                          child: Slidable(
                            actionPane: SlidableDrawerActionPane(),
                            actionExtentRatio: 0.25,
                            child: ListTile(
                              leading: Icon(
                                (operation.type == OperationType.buy)
                                    ? Icons.arrow_circle_down
                                    : Icons.arrow_circle_up,
                                color: (operation.type == OperationType.buy)
                                    ? Colors.greenAccent[400]
                                    : Colors.redAccent[400],
                              ),
                              title: Text(
                                operation.ticker,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              subtitle: Text(DateFormat("dd-MM-yyyy")
                                  .format(operation.date)),
                              trailing: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "R\$ ${operation.price.toStringAsFixed(2)}",
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  Text(
                                    "${operation.qnt.round()}",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                            secondaryActions: [
                              IconSlideAction(
                                caption: 'Editar',
                                color: Colors.blueGrey,
                                icon: Icons.edit,
                                onTap: () => _edit(operation),
                              ),
                              IconSlideAction(
                                caption: 'Excluir',
                                color: Colors.red,
                                icon: Icons.delete,
                                onTap: () => _delete(operation),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  Widget _buildProfitLossChart(List<Operation> operations) {
    final data = _processOperationsUsecase(operations);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SizedBox(
          height: 300,
          width: 700,
          child: ProfitLossChart.fromData(data),
        ),
      ),
    );
  }

  _edit(Operation operation) {
    Modular.to
        .pushNamed('/operation/edit', arguments: operation)
        .then((value) async {
      print(value);
      if (value != null && value) {
        _updateAvPriceUsecase(fii).then((value) {
          setState(() {
            _blocOperations.add(LoadOperationsEvent(widget.fii.ticker));
            fii = value;
          });
        });
      }
    });
  }

  _delete(Operation operation) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text("Excluir"),
          content: SingleChildScrollView(
            child: Text("Deseja excluir a operação?"),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                _removeOperationUsecase(operation);
                final newFii = await _updateAvPriceUsecase(fii);
                setState(() {
                  fii = newFii;
                });
                _blocOperations.add(LoadOperationsEvent(fii.ticker));
                Modular.to.pop();
              },
              child: Text("Sim"),
            ),
            TextButton(
              onPressed: () {
                Modular.to.pop();
              },
              child: Text("Não"),
            ),
          ],
        );
      },
    );
  }
}
