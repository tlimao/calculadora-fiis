import 'dart:ui';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/buy_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/sell_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/repositories/fii_repository_impl.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/edit_operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation_factory.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/errors/operation_errors.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/ui/containers/border_container_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:intl/intl.dart';

class FiisOperationEditWidget extends StatefulWidget {
  final Operation operation;

  const FiisOperationEditWidget({this.operation}) : super();
  @override
  _FiisOperationEditWidgetState createState() =>
      _FiisOperationEditWidgetState();
}

class _FiisOperationEditWidgetState extends State<FiisOperationEditWidget> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<AutoCompleteTextFieldState<Fii>> _key = new GlobalKey();
  final TextEditingController _qnt = TextEditingController();
  final TextEditingController _price = TextEditingController();
  final TextEditingController _fii = new TextEditingController();
  DateTime _date;

  // Usecases
  final _editOperationUsecase = Modular.get<EditOperation>();

  @override
  void initState() {
    _date = widget.operation.date;
    _qnt.text = widget.operation.qnt.toStringAsFixed(2);
    _price.text = widget.operation.price.toStringAsFixed(2);
    _fii.text = widget.operation.ticker;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Editar Operação"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              BorderContainer(
                height: 80,
                child: TextFormField(
                  enabled: false,
                  controller: _fii,
                  decoration: InputDecoration(
                    labelText: "Código",
                    icon: Icon(Icons.business),
                  ),
                ),
              ),
              BorderContainer(
                height: 80,
                child: TextFormField(
                    controller: _qnt,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    decoration: InputDecoration(
                      labelText: "Quantidade",
                      icon: Icon(Icons.shopping_bag),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Coloque uma quantidade!';
                      } else if (double.parse(value) == 0.00) {
                        return 'Coloque uma quantidade válida!';
                      }
                      return null;
                    }),
              ),
              BorderContainer(
                height: 80,
                child: TextFormField(
                  cursorColor: Colors.amberAccent[400],
                  controller: _price,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    CurrencyInputFormatter()
                  ],
                  decoration: InputDecoration(
                    labelText: "Preço",
                    icon: Icon(Icons.monetization_on),
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Coloque um preço!';
                    } else if (double.parse(value) == 0.00) {
                      return 'Coloque um preço válido!';
                    }
                    return null;
                  },
                ),
              ),
              //BorderContainer(child: Text("Calcular Taxas")),
              BorderContainer(height: 80, child: _datePicker()),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MaterialButton(
                  color: Colors.amber,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    width: double.maxFinite,
                    height: 70,
                    child: Center(
                      child: Text(
                        "Salvar",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24,
                        ),
                      ),
                    ),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      submit();
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _datePicker() {
    return Row(
      children: <Widget>[
        Icon(Icons.date_range, color: Colors.black45),
        InkWell(
          child: Padding(
              padding: const EdgeInsetsDirectional.only(start: 16),
              child: Row(children: <Widget>[
                Text(DateFormat('dd/MM/yyyy').format(_date),
                    style: TextStyle(fontSize: 20)),
                Icon(Icons.arrow_drop_down)
              ])),
          onTap: () async {
            DateTime date = await showDatePicker(
              context: context,
              firstDate: DateTime(DateTime.now().year - 15),
              lastDate: DateTime(DateTime.now().year + 5),
              initialDate: _date,
            );

            if (date != null) {
              setState(() {
                _date = date;
              });
            }
          },
        )
      ],
    );
  }

  submit() {
    final operation = OperationFactory.create(
        id: widget.operation.id,
        ticker: _fii.text,
        price: double.parse(_price.text),
        type: widget.operation.type,
        date: _date,
        qnt: double.parse(_qnt.text));

    _editOperationUsecase(operation.id, operation).then((result) {
      Navigator.of(context).pop(true);
    }).catchError((error) {
      String msg;
      switch (error.runtimeType) {
        case OperationError:
          msg = error.msg;
          break;
        case OperationError:
          msg = error.msg;
          break;
        default:
          msg = "Não foi possível realizar a operação";
      }
      _showSnackbar(msg);
    });
  }

  _showSnackbar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }
}

class CurrencyInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    int cents = int.parse(newValue.text.replaceAll(".", "")) % 100;
    int integer = int.parse(newValue.text.replaceAll(".", "")) ~/ 100;

    String formated = (cents > 9) ? "$integer.$cents" : "$integer.0$cents";

    return TextEditingValue(
        text: formated,
        selection: TextSelection.collapsed(offset: formated.length));
  }
}
