import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_bloc.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_events.dart';
import 'package:calculadora_preco_medio/context/fiis/application/state/fiis_list_states.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/export_spreadsheet_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/application/usecases/remove_fii.dart';
import 'package:calculadora_preco_medio/context/fiis/domain/entities/fii.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/repositories/fii_repository_impl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:share/share.dart';

class FiisListWidget extends StatefulWidget {
  @override
  _FiisListWidgetState createState() => _FiisListWidgetState();
}

class _FiisListWidgetState extends State<FiisListWidget> {
  final _bloc = Modular.get<FiisListBloc>();
  final _removeUsecase = Modular.get<RemoveFii>();
  final _exportSpreadSheetUsecase = Modular.get<ExportSpreadsheetFii>();
  final _fiiRepositoryImpl = Modular.get<FiisRepositoryImpl>();

  @override
  void initState() {
    _bloc.add(LoadFiisListEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc,
      initialData: EmptyFiisListState(),
      builder: (context, snapshot) {
        if (snapshot == null || snapshot.data == null) {
          return CircularProgressIndicator();
        } else {
          final fiisList = (snapshot.data as FiisListState).fiisList;
          return ListView.builder(
            itemCount: fiisList.length,
            itemBuilder: (context, index) {
              final fii = fiisList[index];
              return Card(
                color: Colors.blueGrey[100],
                child: Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: ListTile(
                    title: Text(
                      fii.ticker,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(_fiiRepositoryImpl.getName(fii.ticker)),
                    trailing: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "R\$ ${fii.price.toStringAsFixed(2)}",
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            "${fii.qnt.round()}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ]),
                    onTap: () {
                      Modular.navigator
                          .pushNamed(
                            "/info",
                            arguments: fii,
                          )
                          .then((value) => setState(() {
                                _bloc.add(LoadFiisListEvent());
                              }));
                    },
                  ),
                  secondaryActions: [
                    IconSlideAction(
                      caption: 'Exportar',
                      color: Colors.blueGrey,
                      icon: Icons.save,
                      onTap: () => _export(fii),
                    ),
                    IconSlideAction(
                      caption: 'Excluir',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () => _delete(fii),
                    )
                  ],
                ),
              );
            },
          );
        }
      },
    );
  }

  _export(Fii fii) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text("${fii.ticker}"),
          content: SingleChildScrollView(
            child: Text("Exportar o histórico para tabela?"),
          ),
          actions: [
            TextButton(
              onPressed: () async {
                final file = await _exportSpreadSheetUsecase(fii.ticker);
                Share.shareFiles([file.path], text: "Tabela ${fii.ticker}");
                Modular.to.popUntil(ModalRoute.withName('/'));
              },
              child: Text("Sim"),
            ),
            TextButton(
              onPressed: () {
                Modular.to.popUntil(ModalRoute.withName('/'));
              },
              child: Text("Não"),
            ),
          ],
        );
      },
    );
  }

  _delete(Fii fii) {
    showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text("${fii.ticker}"),
          content: SingleChildScrollView(
            child: Text(
                "Deseja excluir o Fii ${fii.ticker} e todo o histórico de operações?"),
          ),
          actions: [
            TextButton(
              onPressed: () {
                _removeUsecase(fii.ticker);
                Modular.to.popUntil(ModalRoute.withName('/'));
              },
              child: Text("Sim"),
            ),
            TextButton(
              onPressed: () {
                Modular.to.popUntil(ModalRoute.withName('/'));
              },
              child: Text("Não"),
            ),
          ],
        );
      },
    );
  }
}
