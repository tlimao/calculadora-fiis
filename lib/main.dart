import 'package:calculadora_preco_medio/app/app_module.dart';
import 'package:calculadora_preco_medio/context/fiis/infra/datasources/fii_dao.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/db/app_db.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() {
  runApp(ModularApp(module: AppModule()));
}
