import 'package:calculadora_preco_medio/cross_cut_concerns/db/app_db.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:intl/intl.dart';

const String OPERATION_TABLE = "operation";
const String OPERATION_TABLE_SQL =
    "CREATE TABLE IF NOT EXISTS $OPERATION_TABLE(" +
        "id     TEXT PRIMARY KEY, " +
        "ticker TEXT NOT NULL, " +
        "qnt    REAL CHECK(qnt > 0) NOT NULL, " +
        "price  REAL CHECK(price >= 0) NOT NULL, " +
        "type   TEXT CHECK( type IN ('buy', 'sell')), " +
        "date   TEXT NOT NULL)";

class OperationDao {
  final AppDataBase _dataBase;

  OperationDao(this._dataBase) {
    _dataBase.createTable(OPERATION_TABLE_SQL);
  }

  insert(Operation operation) async {
    final db = await _dataBase.get();

    db.insert(
      OPERATION_TABLE,
      operation.toMap(),
    );
  }

  delete(Operation operation) async {
    final db = await _dataBase.get();
    db.delete(
      OPERATION_TABLE,
      where: 'id = ?',
      whereArgs: [operation.id],
    );
  }

  deleteAssetOperations(String ticker) async {
    final db = await _dataBase.get();
    db.delete(
      OPERATION_TABLE,
      where: 'ticker = ?',
      whereArgs: [ticker],
    );
  }

  Future<List<Operation>> select(String keyword) async {
    final db = await _dataBase.get();

    List<Map<String, dynamic>> result;

    if (keyword == "*") {
      result = await db.query(OPERATION_TABLE);
    } else {
      result = await db.query(
        OPERATION_TABLE,
        where: 'ticker = ?',
        whereArgs: [keyword],
      );
    }

    List<Operation> operations = List<Operation>();

    result.forEach((element) {
      operations.add(Operation.fromMap(element));
    });

    return operations;
  }

  update({String idOperation, Operation operation}) async {
    final db = await _dataBase.get();
    db.update(
      OPERATION_TABLE,
      {
        'qnt': operation.qnt,
        'price': operation.price,
        'date': DateFormat('yyyy-MM-dd hh:mm:ss').format(operation.date),
      },
      where: 'id = ?',
      whereArgs: [idOperation],
    );
  }
}
