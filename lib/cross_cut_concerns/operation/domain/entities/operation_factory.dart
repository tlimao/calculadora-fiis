import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';

class OperationFactory {
  static Operation create({
    String id,
    OperationType type,
    String ticker,
    double qnt,
    double price,
    DateTime date,
  }) {
    return Operation(
      id: id,
      type: type,
      ticker: ticker,
      qnt: qnt,
      price: price,
      date: date,
    );
  }
}
