import 'package:enum_to_string/enum_to_string.dart';
import 'package:intl/intl.dart';

enum OperationType { sell, buy }

class Operation {
  final String id;
  final OperationType type;
  final String ticker;
  final double qnt;
  final double price;
  final DateTime date;

  Operation({
    this.id,
    this.type,
    this.ticker,
    this.qnt,
    this.price,
    this.date,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'type': EnumToString.convertToString(type),
      'ticker': ticker,
      'qnt': qnt,
      'price': price,
      'date': DateFormat('yyyy-MM-dd hh:mm:ss').format(date)
    };
  }

  factory Operation.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Operation(
      id: map['id'],
      type: EnumToString.fromString(
        OperationType.values,
        map['type'],
      ),
      ticker: map['ticker'],
      qnt: map['qnt'],
      price: map['price'],
      date: DateTime.parse(map['date']),
    );
  }
}
