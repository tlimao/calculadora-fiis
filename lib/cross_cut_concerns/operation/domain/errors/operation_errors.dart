abstract class OperationError {
  final String msg;

  OperationError(this.msg);

  @override
  String toString() {
    return "${this.runtimeType}: ${this.msg}";
  }
}

class OperationNotPerformed extends OperationError {
  OperationNotPerformed({String msg = "Operation not performed!"}) : super(msg);
}

class OperationRemoveError extends OperationError {
  OperationRemoveError({String msg = "Error when remove!"}) : super(msg);
}

class OperationEditError extends OperationError {
  OperationEditError({String msg = "Error when editing!"}) : super(msg);
}
