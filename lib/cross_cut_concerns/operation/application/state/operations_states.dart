import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';

abstract class OperationsState {
  final List<Operation> operations;

  OperationsState(this.operations);
}

class EmptyOperationsState extends OperationsState {
  EmptyOperationsState() : super([]);
}

class NewOperationsState extends OperationsState {
  NewOperationsState(List<Operation> operations) : super(operations);
}

class ErrorOperationsState extends OperationsState {
  ErrorOperationsState() : super([]);
}
