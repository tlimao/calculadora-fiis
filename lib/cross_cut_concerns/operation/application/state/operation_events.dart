abstract class OperationEvent {}

class LoadOperationsEvent extends OperationEvent {
  final String ticker;

  LoadOperationsEvent(this.ticker);
}
