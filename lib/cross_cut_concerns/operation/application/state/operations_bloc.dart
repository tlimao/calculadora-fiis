import 'package:bloc/bloc.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/state/operations_states.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/application/usecases/recover_operations.dart';

import 'operation_events.dart';

class OperationsBloc extends Bloc<OperationEvent, OperationsState> {
  final RecoverOperations _recoverOperationsUseCase;

  OperationsBloc(OperationsState initialState, this._recoverOperationsUseCase)
      : super(initialState);

  @override
  Stream<OperationsState> mapEventToState(OperationEvent event) async* {
    if (event is LoadOperationsEvent) {
      try {
        final operationsList = await _recoverOperationsUseCase(event.ticker);

        if (operationsList.isEmpty) {
          yield EmptyOperationsState();
        } else {
          yield NewOperationsState(operationsList);
        }
      } catch (e) {
        print(e);
        yield ErrorOperationsState();
      }
    }
  }
}
