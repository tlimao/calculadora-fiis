import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/ui/charts/profit_loss_chart.dart';

const List<String> MONTHS = [
  "Jan",
  "Fev",
  "Mar",
  "Abr",
  "Mai",
  "Jun",
  "Jul",
  "Ago",
  "Set",
  "Out",
  "Nov",
  "Dez"
];

class ProcessOperations {
  ProcessOperations();

  List<ProfitLoss> call(List<Operation> operations) {
    // Ordenar por data as operações
    operations.sort((o1, o2) => o1.date.compareTo(o2.date));
    // Iniciar variáveis
    double pm = 0;
    double position = 0;
    List<double> results = List.generate(12, (index) => 0);
    // Iterar sobre as operações
    operations.forEach((operation) {
      if (operation.type == OperationType.buy) {
        pm = (pm * position + operation.price * operation.qnt) /
            (operation.qnt + position);
        position += operation.qnt;
      } else {
        final result = operation.qnt * (operation.price - pm);
        position -= operation.qnt;
        results[operation.date.month - 1] += result;
      }
    });
    // Retornar os resultados
    List<ProfitLoss> profitLoss = [];
    for (var i = 0; i < results.length; i++) {
      profitLoss.add(ProfitLoss(
        MONTHS[i],
        roundDouble(results[i], 2),
        (results[i] > 0) ? roundDouble(results[i] * 0.2, 2) : 0,
      ));
    }

    return profitLoss;
  }
}
