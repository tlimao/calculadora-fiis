import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/errors/operation_errors.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class EditOperation {
  final OperationDao _operationDao;

  EditOperation(this._operationDao);

  call(String idOperation, Operation newOperation) async {
    try {
      await _operationDao.update(
        idOperation: idOperation,
        operation: newOperation,
      );
    } catch (e) {
      throw OperationEditError(msg: "Não foi possível editar a operação!");
    }
  }
}
