import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class RecoverOperations {
  final OperationDao _operationDao;

  RecoverOperations(this._operationDao);

  Future<List<Operation>> call(String keyword) async {
    try {
      return _operationDao.select(keyword);
    } catch (e) {
      return [];
    }
  }
}
