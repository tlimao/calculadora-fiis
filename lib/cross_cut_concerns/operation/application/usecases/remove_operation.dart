import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/errors/operation_errors.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/infra/datasources/operation_dao.dart';

class RemoveOperation {
  final OperationDao _operationDao;

  RemoveOperation(this._operationDao);

  call(Operation operation) async {
    try {
      await _operationDao.delete(operation);
    } catch (e) {
      throw OperationRemoveError(msg: "Erro durante a remoção da operação!");
    }
  }
}
