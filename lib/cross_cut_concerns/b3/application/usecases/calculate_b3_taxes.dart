import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';

class CalculateB3Taxes {
  static double tradingFee = 0.00003219;
  static double settlementFee = 0.000275;
  static double b3Fee = 0.00030719;

  Map<String, double> fromOperation(Operation operation) {
    return fromPriceQnt(
      price: operation.price,
      qnt: operation.qnt,
    );
  }

  Map<String, double> fromPriceQnt({double price, double qnt}) {
    final opTradingFee = roundDouble(tradingFee * (price * qnt), 2);
    final opSettlementFee = roundDouble(settlementFee * (price * qnt), 2);
    final opB3Fee = roundDouble(b3Fee * (price * qnt), 2);
    ;

    return {
      "trading_fee": opTradingFee,
      "settlement_fee": opSettlementFee,
      "b3_fee": opB3Fee
    };
  }
}
