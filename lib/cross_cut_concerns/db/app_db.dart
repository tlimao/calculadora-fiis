import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

const String DATABASE_NAME = "calculadora_preco_medio.db";
const int DATABASE_VERSION = 1;

class AppDataBase {
  Future<Database> _getDatabase() async {
    return openDatabase(join(await getDatabasesPath(), DATABASE_NAME),
        version: DATABASE_VERSION);
  }

  createTable(String sql) async {
    final db = await _getDatabase();
    db.execute(sql);
  }

  dropTable(String table) async {
    final db = await _getDatabase();
    db.delete(table);
  }

  Future<Database> get() async {
    return _getDatabase();
  }
}
