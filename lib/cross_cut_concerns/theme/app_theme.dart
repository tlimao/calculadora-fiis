import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData data = ThemeData(
    brightness: Brightness.light,
    primaryColor: Color(0xFF10385F),
  );
}
