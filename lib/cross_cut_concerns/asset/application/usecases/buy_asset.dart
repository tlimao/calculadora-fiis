import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:calculadora_preco_medio/cross_cut_concerns/operation/domain/entities/operation.dart';

abstract class BuyAsset {
  Map<String, double> calculateAvPrice(Asset asset, Operation operation) {
    final newQnt = asset.qnt + operation.qnt;
    final newAvPrice =
        (asset.price * asset.qnt + operation.price * operation.qnt) / newQnt;

    return {
      "qnt": newQnt,
      "price": roundDouble(newAvPrice, 2),
    };
  }
}
