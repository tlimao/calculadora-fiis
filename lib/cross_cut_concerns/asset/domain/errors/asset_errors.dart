abstract class AssetError {
  final String msg;

  AssetError(this.msg);

  @override
  String toString() {
    return "${this.runtimeType}: ${this.msg}";
  }
}

class AssetNotFound extends AssetError {
  AssetNotFound({String msg = "Asset not found in your Wallet!"}) : super(msg);
}
