import 'dart:math';

import 'package:enum_to_string/enum_to_string.dart';

enum AssetType { Fii, Stock }

abstract class Asset {
  final AssetType type;
  final String ticker;
  final String name;
  final double price;
  final double qnt;
  final String url;

  Asset({
    this.type,
    this.ticker,
    this.name,
    this.price,
    this.qnt,
    this.url,
  });

  Map<String, dynamic> toMap() {
    return {
      'type': EnumToString.convertToString(type),
      'ticker': ticker,
      'name': name,
      'price': price,
      'qnt': qnt,
      'url': url,
    };
  }
}

double roundDouble(double value, int places) {
  double mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}
