import 'package:calculadora_preco_medio/cross_cut_concerns/asset/domain/entities/asset.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class ProfitLossChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  ProfitLossChart(this.seriesList, {this.animate});

  factory ProfitLossChart.fromData(List<ProfitLoss> data) {
    return ProfitLossChart(
      _createData(data),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      seriesList,
      animate: animate,
      vertical: false,
      barRendererDecorator: charts.BarLabelDecorator<String>(),
      domainAxis: charts.OrdinalAxisSpec(),
      barGroupingType: charts.BarGroupingType.stacked,
      behaviors: [
        charts.SeriesLegend(),
        charts.SlidingViewport(
          charts.SelectionModelType.action,
        ),
        charts.PanBehavior(),
      ],
      primaryMeasureAxis: charts.NumericAxisSpec(),
      secondaryMeasureAxis: charts.NumericAxisSpec(),
    );
  }

  static List<charts.Series<ProfitLoss, String>> _createData(
      List<ProfitLoss> data) {
    return [
      charts.Series<ProfitLoss, String>(
        id: 'Prejuízo',
        colorFn: (ProfitLoss profitLoss, _) {
          return charts.MaterialPalette.red.shadeDefault;
        },
        domainFn: (ProfitLoss profitLoss, _) => profitLoss.month,
        measureFn: (ProfitLoss profitLoss, _) =>
            (profitLoss.result < 0) ? profitLoss.result : 0,
        data: data,
        labelAccessorFn: (ProfitLoss profitLoss, _) =>
            (profitLoss.result < 0.0) ? 'R\$ ${profitLoss.result}' : '',
      ),
      charts.Series<ProfitLoss, String>(
        id: 'Lucro líquido',
        colorFn: (ProfitLoss profitLoss, _) {
          return charts.MaterialPalette.green.shadeDefault;
        },
        domainFn: (ProfitLoss profitLoss, _) => profitLoss.month,
        measureFn: (ProfitLoss profitLoss, _) => (profitLoss.result > 0)
            ? roundDouble(profitLoss.result - profitLoss.taxes, 2)
            : 0,
        data: data,
        labelAccessorFn: (ProfitLoss profitLoss, _) => (profitLoss.result > 0.0)
            ? 'R\$ ${roundDouble(profitLoss.result - profitLoss.taxes, 2)}'
            : '',
      ),
      charts.Series<ProfitLoss, String>(
        id: 'Darf (20%)',
        colorFn: (ProfitLoss profitLoss, _) {
          return charts.MaterialPalette.indigo.shadeDefault;
        },
        domainFn: (ProfitLoss profitLoss, _) => profitLoss.month,
        measureFn: (ProfitLoss profitLoss, _) => profitLoss.taxes,
        data: data,
        labelAccessorFn: (ProfitLoss profitLoss, _) =>
            (profitLoss.taxes == 0.0) ? '' : 'R\$ ${profitLoss.taxes}',
      )
    ];
  }
}

class ProfitLoss {
  final String month;
  final double result;
  final double taxes;

  ProfitLoss(this.month, this.result, this.taxes);
}
