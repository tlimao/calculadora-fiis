import 'package:flutter/material.dart';

class BorderContainer extends StatelessWidget {
  final Widget child;
  final double height;
  final double width;
  BorderContainer(
      {this.child, this.height = 70, this.width = double.maxFinite});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 4.0, bottom: 0.0, left: 8.0, right: 8.0),
        padding: const EdgeInsets.all(2.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
        ),
        height: this.height,
        width: this.width,
        child: this.child);
  }
}
